#include "Color.h"

class Pixel
{
public:
	Pixel();
	Pixel(Color color);

	~Pixel();

	Color getColor();
	int getEnergy();
	bool getIsSeam();

	void setColor(Color color);
	void setEnergy(int energy);
	void setIsSeam(bool isSeam);
private:
	Color color;
	int energy;
	bool isSeam;
};