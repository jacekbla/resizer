#include <vector>
#include <windows.h>

int smallestValueIndex(std::vector<int> vec);
std::vector<int> smallestValueIndices(std::vector<int> row); 
bool isPointLeftOfLine(int x, int y, int x1, int y1, int x2, int y2);
std::string dirPath();