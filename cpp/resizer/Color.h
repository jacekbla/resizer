#include <cmath>

class Color
{
public:
	Color();
	Color(int red, int green, int blue);

	~Color();

	Color operator-(Color color1);
	bool operator==(Color color1);

	int getRed();
	int getGreen();
	int getBlue();

	void setRed(int red);
	void setGreen(int green);
	void setBlue(int blue);
private:
	int red;
	int green;
	int blue;
};