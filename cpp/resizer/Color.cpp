#include "Color.h"

Color::Color()
{
	red = 0;
	green = 0;
	blue = 0;
};

Color::Color(int red, int green, int blue)
{
	Color::red = red;
	Color::green = green;
	Color::blue = blue;
};

Color::~Color()
{};

Color Color::operator-(Color color1)
{
	return Color(std::abs(red - color1.getRed()), std::abs(green - color1.getGreen()), std::abs(blue - color1.getBlue()));
};

bool Color::operator==(Color color1)
{
	return (red == color1.getRed() && green == color1.getGreen() && blue == color1.getBlue());
};

int Color::getRed()
{
	return red;
};

int Color::getGreen()
{
	return green;
};

int Color::getBlue()
{
	return blue;
};

void Color::setRed(int red)
{
	Color::red = red;
};

void Color::setGreen(int green)
{
	Color::green = green;
};

void Color::setBlue(int blue)
{
	Color::blue = blue;
};