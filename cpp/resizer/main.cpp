//#include <iostream>
//#include "Image.h"
//
//int strToInt(std::string str)
//{
//	std::stringstream ss(str);
//	int out;
//	ss >> out;
//	return out;
//};
//
//int main(int argc, char const *argv[])
//{
//	std::string inPath = "D:\\test\\cat2.jpg";
//	std::string outPath = "D:\\test\\out.jpg";
//
//	Image img;
//
//	int option = -1;
//	
//	std::cout << "1. Pick img" << std::endl
//			<< "2. Shrink" << std::endl
//			<< "3. Enlarge" << std::endl
//			<< "4. Shrink Seam Carving" << std::endl
//			<< "5. Enlarge Seam Carving" << std::endl
//			<< "6. Test" << std::endl
//			<< "7. Test" << std::endl
//			<< "8. Test" << std::endl
//			<< "9. Test" << std::endl
//			<< "0. Exit" << std::endl << std::endl;
//	while (option != 0)
//	{
//		std::cin >> option;
//		if (option == 1)
//		{
//			std::string path;
//			std::cout << "Path:" << std::endl;
//			std::cin >> path;
//			img.init(path);
//		}
//		else if (option == 6)
//		{
//			img.init("D:\\test\\suru.jpg");
//			int len = 8;
//			int* points = new int[len];
//			points[0] = 200;
//			points[1] = 50;
//
//			points[2] = 300;
//			points[3] = 50;
//
//			points[4] = 300;
//			points[5] = 300;
//
//			points[6] = 200;
//			points[7] = 300;
//
//			img.deleteObj(points, len);
//
//			img.saveImage(outPath);
//			cv::Mat3b img_file = cv::imread(outPath);
//			cv::imshow("image", img_file);
//			cv::waitKey();
//		}
//		else if (option == 7)
//		{
//			img.init("D:\\test\\ppl_forest.jpg");
//			
//			img.makeEnergyImage();
//
//			img.saveImage(outPath);
//			cv::Mat3b img_file = cv::imread(outPath);
//			cv::imshow("image", img_file);
//			cv::waitKey();
//		}
//		else if (option == 8)
//		{
//			std::string original = "D:\\test\\desk\\desk_low.png";
//
//			img.init(original);
//			img.shrinkSC(0.3f * img.getWidth());
//			img.saveImage(original.substr(0, original.find_last_of(".")) + "07sc.png");
//
//			img.init(original);
//			img.shrinkSC(0.5f * img.getWidth());
//			img.saveImage(original.substr(0, original.find_last_of(".")) + "05sc.png");
//
//			img.init(original);
//			img.shrinkSC(0.7f * img.getWidth());
//			img.saveImage(original.substr(0, original.find_last_of(".")) + "03sc.png");
//
//
//			img.init(original);
//			img.crop(new int[4]{ (int)(0.15f * (float)img.getWidth()), 0, (int)(0.85f * (float)img.getWidth()), img.getHeight() });
//			img.saveImage(original.substr(0, original.find_last_of(".")) + "_middle07crop.png");
//
//			img.init(original);
//			img.crop(new int[4]{ (int)(0.25f * (float)img.getWidth()), 0, (int)(0.75f * (float)img.getWidth()), img.getHeight() });
//			img.saveImage(original.substr(0, original.find_last_of(".")) + "_middle05crop.png");
//
//			img.init(original);
//			img.crop(new int[4]{ (int)(0.3f * (float)img.getWidth()), 0, (int)(0.6f * (float)img.getWidth()), img.getHeight() });
//			img.saveImage(original.substr(0, original.find_last_of(".")) + "_middle03crop.png");
//
//			/*
//			img.init(original);
//			img.enlarge(img.getWidth());
//			img.saveImage(original.substr(0, original.find_last_of(".")) + '2' + ".png");
//
//			img.init(original);
//			img.enlarge(0.5f*img.getWidth());
//			img.saveImage(original.substr(0, original.find_last_of(".")) + "15" + ".png");
//
//			img.init(original);
//			img.enlarge(0.2f*img.getWidth());
//			img.saveImage(original.substr(0, original.find_last_of(".")) + "12" + ".png");
//
//			img.init(original);
//			img.shrink(0.3f*img.getWidth());
//			img.saveImage(original.substr(0, original.find_last_of(".")) + "07" + ".png");
//
//			img.init(original);
//			img.shrink(0.5f*img.getWidth());
//			img.saveImage(original.substr(0, original.find_last_of(".")) + "05" + ".png");
//
//			img.init(original);
//			img.shrink(0.7f*img.getWidth());
//			img.saveImage(original.substr(0, original.find_last_of(".")) + "03" + ".png");
//			
//
//
//			img.init(original);
//			img.enlargeSC(img.getWidth());
//			img.saveImage(original.substr(0, original.find_last_of(".")) + "2sc" + ".png");
//
//			img.init(original);
//			img.enlargeSC(0.5f*img.getWidth());
//			img.saveImage(original.substr(0, original.find_last_of(".")) + "15sc" + ".png");
//
//			img.init(original);
//			img.enlargeSC(0.2f*img.getWidth());
//			img.saveImage(original.substr(0, original.find_last_of(".")) + "12sc" + ".png");
//
//			img.init(original);
//			img.shrinkSC(0.3f*img.getWidth());
//			img.saveImage(original.substr(0, original.find_last_of(".")) + "07sc" + ".png");
//
//			img.init(original);
//			img.shrinkSC(0.5f*img.getWidth());
//			img.saveImage(original.substr(0, original.find_last_of(".")) + "05sc" + ".png");
//
//			img.init(original);
//			img.shrinkSC(0.7f*img.getWidth());
//			img.saveImage(original.substr(0, original.find_last_of(".")) + "03sc" + ".png");
//			*/
//		}
//		else if (option == 9)
//		{
//			img.init("D:\\test\\yoga.jpg");
//
//			img.shrinkForwardSC(300);
//
//			img.saveImage(outPath);
//			cv::Mat3b img_file = cv::imread(outPath);
//			cv::imshow("image", img_file);
//			cv::waitKey();
//		}
//		else if(option != 0)
//		{
//			std::string diff;
//			std::cout << "How many pixels:" << std::endl;
//			std::cin >> diff;
//
//			std::string axis;
//			std::cout << "Horizontal(h)/Vertical(v):" << std::endl;
//			std::cin >> axis;
//
//			if (axis == "h")
//			{
//				if (option == 2)
//				{
//					img.shrink(strToInt(diff));
//				}
//				if (option == 3)
//				{
//					img.enlarge(strToInt(diff));
//				}
//				if (option == 4)
//				{
//					img.shrinkSC(strToInt(diff));
//				}
//				if (option == 5)
//				{
//					img.enlargeSC(strToInt(diff));
//				}
//			}
//			else
//			{
//
//				if (option == 2)
//				{
//					img.shrinkVertical(strToInt(diff));
//				}
//				if (option == 3)
//				{
//					img.enlargeVertical(strToInt(diff));
//				}
//				if (option == 4)
//				{
//					img.shrinkVerticalSC(strToInt(diff));
//				}
//				if (option == 5)
//				{
//					img.enlargeVerticalSC(strToInt(diff));
//				}
//			}
//
//			img.saveImage(outPath);
//			cv::Mat3b img_file = cv::imread(outPath);
//			cv::imshow("image", img_file);
//			cv::waitKey();
//		}
//	}
//
//	return 0;
//}