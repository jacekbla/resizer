#include "Utility.h"

int smallestValueIndex(std::vector<int> vec)
{
	int smallest = (std::numeric_limits<int>::max)();
	int smallestIndex = -1;
	for (int i = 0; i < vec.size(); i++)
	{
		if (vec[i] < smallest)
		{
			smallest = vec[i];
			smallestIndex = i;
		}
	}
	return smallestIndex;
};

std::vector<int> smallestValueIndices(std::vector<int> row)
{
	std::vector<int> out;

	for(int i = 0; i < row.size(); i++)
	{
		int smallest = smallestValueIndex(row);
		out.push_back(smallest);
		row[smallest] = (std::numeric_limits<int>::max)();
	}

	return out;
};

bool isPointLeftOfLine(int x, int y, int x1, int y1, int x2, int y2)
{
	int biggerY = y1 > y2 ? y1 : y2;
	int smallerY = y1 > y2 ? y2 : y1;

	if (y < smallerY || y > biggerY)
	{
		return false;
	}

	if (x1 != x2)
	{
		float a = (float)((float)y1 - (float)y2) / ((float)x1 - (float)x2);
		float b = (float)(((float)x1 * (float)y2) - ((float)x2 * (float)y1)) / ((float)x1 - (float)x2);

		float xOnLine = ((float)y - b) / a;

		return xOnLine > (float)x;
	}
	else
	{
		return x <= x1;
	}
};

std::string dirPath()
{
	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	return std::string(buffer).substr(0, std::string(buffer).find_last_of("\\/"));
};