#include <windows.h>
#define WIN32_LEAN_AND_MEAN
#include <jni.h>
#include "Image.h"

std::string resize(std::string path, int width, int height)
{
	Image img;
	img.init(path);

	int diffWidth = width - img.getWidth();
	int diffHeight = height - img.getHeight();

	if (diffWidth < 0)
	{
		img.shrink(std::abs(diffWidth));
	}
	else
	{
		img.enlarge(diffWidth);
	}

	if (diffHeight < 0)
	{
		img.shrinkVertical(std::abs(diffHeight));
	}
	else
	{
		img.enlargeVertical(diffHeight);
	}
	std::string outPath = path.substr(0, path.find_last_of("\\/")) + "\\out" + path.substr(path.find_last_of("."), path.length());
	img.saveImage(outPath);
	return outPath;
};

std::string resizeSC(std::string path, int width, int height)
{
	Image img;
	img.init(path);

	int diffWidth = width - img.getWidth();
	int diffHeight = height - img.getHeight();

	if (diffWidth < 0)
	{
		img.shrinkSC(std::abs(diffWidth));
	}
	else
	{
		img.enlargeSC(diffWidth);
	}

	if (diffHeight < 0)
	{
		img.shrinkVerticalSC(std::abs(diffHeight));
	}
	else
	{
		img.enlargeVerticalSC(diffHeight);
	}
	std::string outPath = path.substr(0, path.find_last_of("\\/")) + "\\out" + path.substr(path.find_last_of("."), path.length());
	img.saveImage(outPath);
	return outPath;
};

std::string resizeForwardSC(std::string path, int width, int height)
{
	Image img;
	img.init(path);

	int diffWidth = width - img.getWidth();
	int diffHeight = height - img.getHeight();

	if (diffWidth < 0)
	{
		img.shrinkForwardSC(std::abs(diffWidth));
	}
	else
	{
		img.enlargeForwardSC(diffWidth);
	}

	if (diffHeight < 0)
	{
		img.shrinkVerticalForwardSC(std::abs(diffHeight));
	}
	else
	{
		img.enlargeVerticalForwardSC(diffHeight);
	}
	std::string outPath = path.substr(0, path.find_last_of("\\/")) + "\\out" + path.substr(path.find_last_of("."), path.length());
	img.saveImage(outPath);
	return outPath;
};

std::string crop(std::string path, int* points)
{
	Image img;
	img.init(path);

	img.crop(points);

	std::string outPath = path.substr(0, path.find_last_of("\\/")) + "\\out" + path.substr(path.find_last_of("."), path.length());
	img.saveImage(outPath);
	
	return outPath;
};

std::string deleteObj(std::string path, int* points, int length)
{
	std::string outPath = path;
	if (length > 0)
	{
		Image img;
		img.init(path);

		img.deleteObj(points, length);

		outPath = path.substr(0, path.find_last_of("\\/")) + "\\out" 
			+ path.substr(path.find_last_of("."), path.length());
		img.saveImage(outPath);
	}
	return outPath;
};

std::string jstring2string(JNIEnv *env, jstring jStr)
{
	if (!jStr)
	{
		return "";
	}

	const jclass stringClass = env->GetObjectClass(jStr);
	const jmethodID getBytes = env->GetMethodID(stringClass, "getBytes", "(Ljava/lang/String;)[B");
	const jbyteArray stringJbytes = (jbyteArray)env->CallObjectMethod(jStr, getBytes, env->NewStringUTF("UTF-8"));

	size_t length = (size_t)env->GetArrayLength(stringJbytes);
	jbyte* pBytes = env->GetByteArrayElements(stringJbytes, NULL);

	std::string ret = std::string((char *)pBytes, length);
	env->ReleaseByteArrayElements(stringJbytes, pBytes, JNI_ABORT);

	env->DeleteLocalRef(stringJbytes);
	env->DeleteLocalRef(stringClass);
	return ret;
};

extern "C" {
	/*
	* Class:     resizer_Controller
	* Method:    cppResize
	* Signature: (Ljava/lang/String;II)Ljava/lang/String;
	*/
	JNIEXPORT jstring JNICALL Java_resizer_Controller_cppResize
	(JNIEnv *env, jobject obj, jstring path, jint width, jint height)
	{
		const char *str = env->GetStringUTFChars(path, 0);
		return env->NewStringUTF(resize(str, width, height).c_str());
	};

	/*
	 * Class:     resizer_Controller
	 * Method:    cppResizeSC
	 * Signature: (Ljava/lang/String;II)Ljava/lang/String;
	 */
	JNIEXPORT jstring JNICALL Java_resizer_Controller_cppResizeSC
	(JNIEnv *env, jobject obj, jstring path, jint width, jint height)
	{
		const char *str = env->GetStringUTFChars(path, 0);
		return env->NewStringUTF(resizeSC(str, width, height).c_str());
	};

	/*
	 * Class:     resizer_Controller
	 * Method:    cppResizeForwardSC
	 * Signature: (Ljava/lang/String;II)Ljava/lang/String;
	 */
	JNIEXPORT jstring JNICALL Java_resizer_Controller_cppResizeForwardSC
	(JNIEnv *env, jobject obj, jstring path, jint width, jint height)
	{
		const char *str = env->GetStringUTFChars(path, 0);
		return env->NewStringUTF(resizeForwardSC(str, width, height).c_str());
	};

	/*
	 * Class:     resizer_Controller
	 * Method:    cppCrop
	 * Signature: (Ljava/lang/String;[I)Ljava/lang/String;
	 */
	JNIEXPORT jstring JNICALL Java_resizer_Controller_cppCrop
	(JNIEnv *env, jobject obj, jstring path, jintArray points)
	{
		const char *str = env->GetStringUTFChars(path, 0);
		const jsize length = env->GetArrayLength(points);
		jint* arr = env->GetIntArrayElements(points, NULL);
		env->ReleaseIntArrayElements(points, arr, NULL);
		int* cppArr = new int[length];
		for (int i = 0; i < length; i++)
		{
			cppArr[i] = arr[i];
		}
		return env->NewStringUTF(crop(str, cppArr).c_str());
	};

	/*
	 * Class:     resizer_Controller
	 * Method:    cppDeleteObj
	 * Signature: (Ljava/lang/String;[I)Ljava/lang/String;
	 */
	JNIEXPORT jstring JNICALL Java_resizer_Controller_cppDeleteObj
	(JNIEnv *env, jobject obj, jstring path, jintArray points)
	{
		const char *str = env->GetStringUTFChars(path, 0);
		const jsize length = env->GetArrayLength(points);
		jint* arr = env->GetIntArrayElements(points, NULL);
		env->ReleaseIntArrayElements(points, arr, NULL);
		int* cppArr = new int[length];
		for (int i = 0; i < length; i++)
		{
			cppArr[i] = arr[i];
		}
		return env->NewStringUTF(deleteObj(str, cppArr, length).c_str());
	};
}




BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}
