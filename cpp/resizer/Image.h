#include "Utility.h"
#include "Pixel.h"
#include <string>
#include <opencv2/opencv.hpp>

class Image
{
public:
	Image();
	Image(std::vector<std::vector<Pixel>> pixels, int width, int height);

	~Image();

	int getWidth();
	int getHeight();
	void setWidth(int width);
	void setHeight(int height);

	void init(std::string path);
	void saveImage(std::string path);

	void shrink(int diff);
	void shrinkVertical(int diff);
	void enlarge(int diff);
	void enlargeVertical(int diff);
	void shrinkSC(int diff);
	void shrinkVerticalSC(int diff);
	void enlargeSC(int diff);
	void enlargeVerticalSC(int diff);
	void shrinkForwardSC(int diff);
	void shrinkVerticalForwardSC(int diff);
	void enlargeForwardSC(int diff);
	void enlargeVerticalForwardSC(int diff);

	void makeEnergyImage();
	void makeSeamedImage(int amount);

	void crop(int* points);

	void deleteObj(int* points, int length);
private:
	std::vector<std::vector<Pixel>> pixels;
	int width;
	int height;
	int deletionEnergy;

	void loadImage(std::string path);
	void computeEnergy();
	void updateEnergy(int x, int y);
	Pixel interpolate(Pixel& pixel1, Pixel& pixel2);
	void drawSeams(std::vector<std::vector<int>> seams);
	void drawSeam(std::vector<int> seam);
	std::vector<std::vector<int>> generateEnergyMatrix();
	std::vector<std::vector<int>> generateVerticalEnergyMatrix();
	std::vector<std::vector<int>> generateForwardEnergyMatrix();
	std::vector<std::vector<int>> generateForwardVerticalEnergyMatrix();

	//shrinkSC
	std::vector<int> generateSeam();
	void deleteSeam(std::vector<int> seam);
	//shrinkVerticalSC
	std::vector<int> generateVerticalSeam();
	void deleteVerticalSeam(std::vector<int> seam);
	//enlargeSC
	std::vector<std::vector<int>> generateSeams(int diff);
	void addSeam(std::vector<int> seam);
	void addSeams(std::vector<std::vector<int>> seams);
	//enlargeVerticalSC
	std::vector<std::vector<int>> generateVerticalSeams(int diff);
	void addVerticalSeam(std::vector<int> seam);
	void addVerticalSeams(std::vector<std::vector<int>> seams);

	//shrinkForwardSC
	std::vector<int> generateSeamForward();
	//shrinkVerticalForwardSC
	std::vector<int> generateVerticalSeamForward();
	//enlargeForwardSC
	std::vector<std::vector<int>> generateSeamsForward(int diff);
	//enlargeVerticalForwardSC
	std::vector<std::vector<int>> generateVerticalSeamsForward(int diff);
};