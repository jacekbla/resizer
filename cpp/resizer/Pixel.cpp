#include "Pixel.h"

Pixel::Pixel() : Pixel(Color())
{};

Pixel::Pixel(Color color)
{
	Pixel::color = color;
	Pixel::energy = 0;
	Pixel::isSeam = false;
};

Pixel::~Pixel()
{};

Color Pixel::getColor()
{
	return color;
};

int Pixel::getEnergy()
{
	return energy;
};

bool Pixel::getIsSeam()
{
	return isSeam;
};

void Pixel::setColor(Color color)
{
	Pixel::color = color;
};

void Pixel::setEnergy(int energy)
{
	Pixel::energy = energy;
};

void Pixel::setIsSeam(bool isSeam)
{
	Pixel::isSeam = isSeam;
};