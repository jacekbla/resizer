#include "Image.h"

Image::Image() : Image(std::vector<std::vector<Pixel>>(), 0, 0)
{};

Image::Image(std::vector<std::vector<Pixel>> pixels, int width, int height)
{
	Image::pixels = pixels;
	Image::width = width;
	Image::height = height; 
	deletionEnergy = -100000;
};

Image::~Image()
{};

int Image::getWidth()
{
	return width;
};

int Image::getHeight()
{
	return height;
};

void Image::setWidth(int width)
{
	Image::width = width;
};

void Image::setHeight(int height)
{
	Image::height = height;
};

void Image::init(std::string path)
{
	loadImage(path);
	computeEnergy();
}

void Image::saveImage(std::string path)
{
	cv::Mat3b img(Image::height, Image::width, cv::Vec3b(0, 0, 0));

	for (int y = 0; y < Image::height; y++)
	{
		for (int x = 0; x < Image::width; x++)
		{
			cv::Vec3b & pixel = img.at<cv::Vec3b>(y, x);
			pixel[0] = Image::pixels[y][x].getColor().getBlue();
			pixel[1] = Image::pixels[y][x].getColor().getGreen();
			pixel[2] = Image::pixels[y][x].getColor().getRed();
		}
	}
	cv::imwrite(path, img);
};

void Image::shrink(int diff)
{
	float step = ((float)Image::width / (float)diff);
	for (int x = 0; x < diff; x++)
	{
		for (int y = 0; y < Image::height; y++)
		{
			Image::pixels[y].erase(Image::pixels[y].begin() + (((float)x*step) - x));
		}
	}
	Image::width -= diff;
};

void Image::shrinkVertical(int diff)
{
	float step = ((float)Image::height / (float)diff);
	for (int y = 0; y < diff; y++)
	{
		Image::pixels.erase(Image::pixels.begin() + (((float)y*step) - y));
	}
	Image::height -= diff;
};

void Image::enlarge(int diff)
{
	float step = ((float)Image::width / (float)diff);
	for (int x = 0; x < diff; x++)
	{
		int index = (((float)x*step) + x);
		std::vector<Pixel> toInsert;
		for (int y = 0; y < Image::height; y++)
		{
			if (index == 0)
			{
				Image::pixels[y].insert(Image::pixels[y].begin() + index, Image::pixels[y][index]);
			}
			else
			{
				Image::pixels[y].insert(Image::pixels[y].begin() + index, 
					interpolate(Image::pixels[y][index - 1], Image::pixels[y][index]));
			}
		}
	}
	Image::width += diff;
};

void Image::enlargeVertical(int diff)
{
	float step = ((float)Image::height / (float)diff);
	for (int y = 0; y < diff; y++)
	{
		int index = (((float)y*step) + y);
		std::vector<Pixel> toInsert;
		for (int x = 0; x < Image::width; x++)
		{
			if (index == 0)
			{
				toInsert.push_back(Image::pixels[index][x]);
			}
			else
			{
				toInsert.push_back(interpolate(Image::pixels[index - 1][x], Image::pixels[index][x]));
			}
		}
		Image::pixels.insert(Image::pixels.begin() + index, toInsert);
	}
	Image::height += diff;
};

void Image::shrinkSC(int diff)
{
	for (int i = 0; i < diff; i++)
	{
		deleteSeam(generateSeam());
	}
}

void Image::shrinkVerticalSC(int diff)
{
	for (int i = 0; i < diff; i++)
	{
		deleteVerticalSeam(generateVerticalSeam());
	}
};

void Image::enlargeSC(int diff)
{
	int count = diff;
	while (count > 0)
	{
		std::vector<std::vector<int>> seams = generateSeams(count);
		addSeams(seams);
		count -= seams.size();
	}
};

void Image::enlargeVerticalSC(int diff)
{
	int count = diff;
	while (count > 0)
	{
		std::cout << count << std::endl;
		std::vector<std::vector<int>> seams = generateVerticalSeams(count);
		addVerticalSeams(seams);
		count -= seams.size();
	}
};

void Image::shrinkForwardSC(int diff)
{
	for (int i = 0; i < diff; i++)
	{
		deleteSeam(generateSeamForward());
	}
};

void Image::shrinkVerticalForwardSC(int diff)
{
	for (int i = 0; i < diff; i++)
	{
		deleteVerticalSeam(generateVerticalSeamForward());
	}
};

void Image::enlargeForwardSC(int diff)
{
	int count = diff;
	while (count > 0)
	{
		std::cout << count << std::endl;
		std::vector<std::vector<int>> seams = generateSeamsForward(count);
		addSeams(seams);
		count -= seams.size();
	}
};

void Image::enlargeVerticalForwardSC(int diff)
{
	int count = diff;
	while (count > 0)
	{
		std::cout << count << std::endl;
		std::vector<std::vector<int>> seams = generateVerticalSeamsForward(count);
		addVerticalSeams(seams);
		count -= seams.size();
	}
};

void Image::makeEnergyImage()
{
	int biggestEnergy = -std::numeric_limits<int>::max();
	for (int y = 0; y < Image::height; y++)
	{
		for (int x = 0; x < Image::width; x++)
		{
			if (pixels[y][x].getEnergy() > biggestEnergy)
			{
				biggestEnergy = pixels[y][x].getEnergy();
			}
		}
	}

	for (int y = 0; y < Image::height; y++)
	{
		for (int x = 0; x < Image::width; x++)
		{
			pixels[y][x].setColor(Color(((float)pixels[y][x].getEnergy() / (float)biggestEnergy) * 255,
				((float)pixels[y][x].getEnergy() / (float)biggestEnergy) * 255, ((float)pixels[y][x].getEnergy() / (float)biggestEnergy) * 255));
		}
	}
};

void Image::makeSeamedImage(int amount)
{
	int count = amount;
	while (count > 0)
	{
		std::vector<std::vector<int>> seams = generateSeams(count);
		drawSeams(seams);
		count -= seams.size();
	}
};

void Image::crop(int* points)
{
	int biggestX = points[0] > points[2] ? points[0] : points[2];
	int smallestX = points[0] > points[2] ? points[2] : points[0];
	int biggestY = points[1] > points[3] ? points[1] : points[3];
	int smallestY = points[1] > points[3] ? points[3] : points[1];

	int newWidth = biggestX - smallestX;
	int newHeight = biggestY - smallestY;
	std::vector<std::vector<Pixel>> newPixels;
	newPixels.resize(newHeight);
	for (int i = 0; i < newHeight; i++)
	{
		newPixels[i].resize(newWidth);
	}

	for (int y = smallestY, newY = 0; y < biggestY; y++, newY++)
	{
		for (int x = smallestX, newX = 0; x < biggestX; x++, newX++)
		{
			newPixels[newY][newX] = pixels[y][x];
		}
	}
	pixels = newPixels;
	width = newWidth;
	height = newHeight;
};

void Image::deleteObj(int* points, int length)
{
	int biggestX = -std::numeric_limits<int>::max();
	int smallestX = std::numeric_limits<int>::max();
	int biggestY = -std::numeric_limits<int>::max();
	int smallestY = std::numeric_limits<int>::max();

	for (int i = 0; i < length; i += 2)
	{
		if (points[i] > biggestX)
		{
			biggestX = points[i];
		}
		if (points[i] < smallestX)
		{
			smallestX = points[i];
		}
		if (points[i + 1] > biggestY)
		{
			biggestY = points[i + 1];
		}
		if (points[i + 1] < smallestY)
		{
			smallestY = points[i + 1];
		}
	}

	for (int y = smallestY; y < biggestY + 1; y++)
	{
		for (int x = smallestX; x < biggestX + 1; x++)
		{
			bool isInside = false;
			for (int i = 0; i < length - 2; i += 2)
			{
				if (isPointLeftOfLine(x, y, points[i], points[i + 1], points[i + 2], points[i + 3]))
				{
					isInside = !isInside;
				}
			}
			if (isPointLeftOfLine(x, y, points[length - 2], points[length - 1], points[0], points[1]))
			{
				isInside = !isInside;
			}
			if (isInside)
			{
				pixels[y][x].setEnergy(deletionEnergy);
				//pixels[y][x].setColor(Color(255,0,0));
			}
		}
	}
	shrinkSC(biggestX - smallestX);
};

void Image::loadImage(std::string path)
{
	if (pixels.size() > 0)
	{
		pixels.clear();
	}

	cv::Mat3b img = cv::imread(path);

	int width = img.cols;
	int height = img.rows;

	Image::width = width;
	Image::height = height;

	for (int y = 0; y < Image::height; y++)
	{
		std::vector<Pixel> row;
		for (int x = 0; x < Image::width; x++)
		{
			cv::Vec3b pixel = img.at<cv::Vec3b>(y, x);
			row.push_back(Pixel(Color(pixel[2], pixel[1], pixel[0])));
		}
		Image::pixels.push_back(row);
	}
};

void Image::computeEnergy()
{
	{
		Color colDiff1 = pixels[0][0].getColor() - pixels[1][0].getColor();
		Color colDiff2 = pixels[0][0].getColor() - pixels[0][1].getColor();

		int diffSum1 = colDiff1.getRed() + colDiff1.getGreen() + colDiff1.getBlue();
		int diffSum2 = colDiff2.getRed() + colDiff2.getGreen() + colDiff2.getBlue();

		pixels[0][0].setEnergy(diffSum1 + diffSum2);
	}

	{
		Color colDiff1 = pixels[height - 1][0].getColor() - pixels[height - 2][0].getColor();
		Color colDiff2 = pixels[height - 1][0].getColor() - pixels[height - 1][1].getColor();

		int diffSum1 = colDiff1.getRed() + colDiff1.getGreen() + colDiff1.getBlue();
		int diffSum2 = colDiff2.getRed() + colDiff2.getGreen() + colDiff2.getBlue();

		pixels[height - 1][0].setEnergy(diffSum1 + diffSum2);
	}

	/*{
		Color colDiff1 = pixels[0][width - 1].getColor() - pixels[1][width - 1].getColor();
		Color colDiff2 = pixels[0][width - 1].getColor() - pixels[0][width - 2].getColor();

		int diffSum1 = colDiff1.getRed() + colDiff1.getGreen() + colDiff1.getBlue();
		int diffSum2 = colDiff2.getRed() + colDiff2.getGreen() + colDiff2.getBlue();

		pixels[0][width - 1].setEnergy(diffSum1 + diffSum2);
	}

	{
		Color colDiff1 = pixels[height - 1][width - 1].getColor() - pixels[height - 2][width - 1].getColor();
		Color colDiff2 = pixels[height - 1][width - 1].getColor() - pixels[height - 1][width - 2].getColor();

		int diffSum1 = colDiff1.getRed() + colDiff1.getGreen() + colDiff1.getBlue();
		int diffSum2 = colDiff2.getRed() + colDiff2.getGreen() + colDiff2.getBlue();

		pixels[height - 1][width - 1].setEnergy(diffSum1 + diffSum2);
	}*/

	for (int i = 1; i < height; i++)
	{
		Color colDiff1 = pixels[i][0].getColor() - pixels[i - 1][0].getColor();
		Color colDiff2 = pixels[i][0].getColor() - pixels[i][1].getColor();

		int diffSum1 = colDiff1.getRed() + colDiff1.getGreen() + colDiff1.getBlue();
		int diffSum2 = colDiff2.getRed() + colDiff2.getGreen() + colDiff2.getBlue();

		pixels[i][0].setEnergy(diffSum1 + diffSum2);
	}

	for (int i = 1; i < width; i++)
	{
		Color colDiff1 = pixels[0][i].getColor() - pixels[0][i - 1].getColor();
		Color colDiff2 = pixels[0][i].getColor() - pixels[1][i].getColor();

		int diffSum1 = colDiff1.getRed() + colDiff1.getGreen() + colDiff1.getBlue();
		int diffSum2 = colDiff2.getRed() + colDiff2.getGreen() + colDiff2.getBlue();

		pixels[0][i].setEnergy(diffSum1 + diffSum2);
	}

	/*for (int i = 1; i < height; i++)
	{
		Color colDiff1 = pixels[i][width - 1].getColor() - pixels[i - 1][width - 1].getColor();
		Color colDiff2 = pixels[i][width - 1].getColor() - pixels[i][width - 2].getColor();

		int diffSum1 = colDiff1.getRed() + colDiff1.getGreen() + colDiff1.getBlue();
		int diffSum2 = colDiff2.getRed() + colDiff2.getGreen() + colDiff2.getBlue();

		pixels[i][width - 1].setEnergy(diffSum1 + diffSum2);
	}

	for (int i = 1; i < width; i++)
	{
		Color colDiff1 = pixels[height - 1][i].getColor() - pixels[height - 1][i - 1].getColor();
		Color colDiff2 = pixels[height - 1][i].getColor() - pixels[height - 2][i].getColor();

		int diffSum1 = colDiff1.getRed() + colDiff1.getGreen() + colDiff1.getBlue();
		int diffSum2 = colDiff2.getRed() + colDiff2.getGreen() + colDiff2.getBlue();

		pixels[height - 1][i].setEnergy(diffSum1 + diffSum2);
	}*/

	for (int i = 1; i < height/* - 1*/; i++)
	{
		for (int j = 1; j < width/* - 1*/; j++)
		{
			Color colDiff1 = pixels[i][j].getColor() - pixels[i - 1][j].getColor();
			Color colDiff2 = pixels[i][j].getColor() - pixels[i][j - 1].getColor();

			int diffSum1 = colDiff1.getRed() + colDiff1.getGreen() + colDiff1.getBlue();
			int diffSum2 = colDiff2.getRed() + colDiff2.getGreen() + colDiff2.getBlue();

			pixels[i][j].setEnergy(diffSum1 + diffSum2);
		}
	}
};

void Image::updateEnergy(int x, int y)
{
	if(x == 0 && y == 0)
	{
		Color colDiff1 = pixels[0][0].getColor() - pixels[1][0].getColor();
		Color colDiff2 = pixels[0][0].getColor() - pixels[0][1].getColor();

		int diffSum1 = colDiff1.getRed() + colDiff1.getGreen() + colDiff1.getBlue();
		int diffSum2 = colDiff2.getRed() + colDiff2.getGreen() + colDiff2.getBlue();

		pixels[0][0].setEnergy(diffSum1 + diffSum2);
	}
	else if(x == 0)
	{
		Color colDiff1 = pixels[y][0].getColor() - pixels[y - 1][0].getColor();
		Color colDiff2 = pixels[y][0].getColor() - pixels[y][1].getColor();

		int diffSum1 = colDiff1.getRed() + colDiff1.getGreen() + colDiff1.getBlue();
		int diffSum2 = colDiff2.getRed() + colDiff2.getGreen() + colDiff2.getBlue();

		pixels[y][0].setEnergy(diffSum1 + diffSum2);
	}
	else if(y == 0)
	{
		Color colDiff1 = pixels[0][x].getColor() - pixels[0][x - 1].getColor();
		Color colDiff2 = pixels[0][x].getColor() - pixels[1][x].getColor();

		int diffSum1 = colDiff1.getRed() + colDiff1.getGreen() + colDiff1.getBlue();
		int diffSum2 = colDiff2.getRed() + colDiff2.getGreen() + colDiff2.getBlue();

		pixels[0][x].setEnergy(diffSum1 + diffSum2);
	}
	else
	{
		Color colDiff1 = pixels[y][x].getColor() - pixels[y - 1][x].getColor();
		Color colDiff2 = pixels[y][x].getColor() - pixels[y][x - 1].getColor();

		int diffSum1 = colDiff1.getRed() + colDiff1.getGreen() + colDiff1.getBlue();
		int diffSum2 = colDiff2.getRed() + colDiff2.getGreen() + colDiff2.getBlue();

		pixels[y][x].setEnergy(diffSum1 + diffSum2);
	}
};

Pixel Image::interpolate(Pixel& pixel1, Pixel& pixel2)
{
	int red = (pixel1.getColor().getRed() + pixel2.getColor().getRed()) / 2;
	int green = (pixel1.getColor().getGreen() + pixel2.getColor().getGreen()) / 2;
	int blue = (pixel1.getColor().getBlue() + pixel2.getColor().getBlue()) / 2;

	return Pixel(Color(red, green, blue));
};

void Image::drawSeams(std::vector<std::vector<int>> seams)
{
	for (int i = 0; i < seams.size(); i++)
	{
		drawSeam(seams[i]);
	}
};

void Image::drawSeam(std::vector<int> seam)
{
	for (int y = 0; y < Image::height; y++)
	{
		int id = seam[seam.size() - 1 - y];
		pixels[y][id].setColor(Color(255,0,0));
	}
};

std::vector<std::vector<int>> Image::generateEnergyMatrix()
{
	std::vector<std::vector<int>> energyMatrix;
	std::vector<int> firstRow;
	for (int x = 0; x < Image::width; x++)
	{
		firstRow.push_back(pixels[0][x].getEnergy());
	}
	energyMatrix.push_back(firstRow);

	for (int y = 1; y < Image::height; y++)
	{
		std::vector<int> nextRow;

		for (int x = 0; x < Image::width; x++)
		{
			int currEnergy = pixels[y][x].getEnergy();
			int accumEnergy;
			if (x == 0)
			{
				accumEnergy = std::min(energyMatrix[y - 1][x], energyMatrix[y - 1][x + 1]);
			}
			else if (x == Image::width - 1)
			{
				accumEnergy = std::min(energyMatrix[y - 1][x - 1], energyMatrix[y - 1][x]);
			}
			else
			{
				accumEnergy = std::min(energyMatrix[y - 1][x - 1], std::min(energyMatrix[y - 1][x], energyMatrix[y - 1][x + 1]));
			}
			currEnergy += accumEnergy;

			nextRow.push_back(currEnergy);
		}

		energyMatrix.push_back(nextRow);
	}

	return energyMatrix;
};

std::vector<std::vector<int>> Image::generateVerticalEnergyMatrix()
{
	std::vector<std::vector<int>> energyMatrix;

	for (int y = 0; y < Image::height; y++)
	{
		std::vector<int> nextRow;
		nextRow.push_back(pixels[y][0].getEnergy());
		energyMatrix.push_back(nextRow);
	}

	for (int x = 1; x < Image::width; x++)
	{
		for (int y = 0; y < Image::height; y++)
		{
			int currEnergy = pixels[y][x].getEnergy();
			int accumEnergy;
			if (y == 0)
			{
				accumEnergy = std::min(energyMatrix[y + 1][x - 1], energyMatrix[y][x - 1]);
			}
			else if (y == Image::height - 1)
			{
				accumEnergy = std::min(energyMatrix[y - 1][x - 1], energyMatrix[y][x - 1]);
			}
			else
			{
				accumEnergy = std::min(energyMatrix[y - 1][x - 1], std::min(energyMatrix[y][x - 1], energyMatrix[y + 1][x - 1]));
			}
			currEnergy += accumEnergy;

			energyMatrix[y].push_back(currEnergy);
		}
	}

	return energyMatrix;
};

std::vector<std::vector<int>> Image::generateForwardEnergyMatrix()
{
	std::vector<std::vector<int>> energyMatrix;
	std::vector<int> firstRow;
	for (int x = 0; x < Image::width; x++)
	{
		firstRow.push_back(pixels[0][x].getEnergy());
	}
	energyMatrix.push_back(firstRow);

	for (int y = 1; y < Image::height; y++)
	{
		std::vector<int> nextRow;

		for (int x = 0; x < Image::width; x++)
		{
			int accumEnergy = 0;
			if (x == 0)
			{
				accumEnergy = std::min(energyMatrix[y - 1][x], energyMatrix[y - 1][x + 1]) + pixels[y][x].getEnergy();
			}
			else if (x == Image::width - 1)
			{
				accumEnergy = std::min(energyMatrix[y - 1][x - 1], energyMatrix[y - 1][x]) + pixels[y][x].getEnergy();
			}
			else
			{
				int Ml = energyMatrix[y - 1][x - 1];
				int Mu = energyMatrix[y - 1][x];
				int Mr = energyMatrix[y - 1][x + 1];

				int Cl = abs(pixels[y][x - 1].getEnergy() - pixels[y][x + 1].getEnergy()) + 
					abs(pixels[y][x - 1].getEnergy() - pixels[y - 1][x].getEnergy());
				int Cu = abs(pixels[y][x - 1].getEnergy() - pixels[y][x + 1].getEnergy());
				int Cr = abs(pixels[y][x - 1].getEnergy() - pixels[y][x + 1].getEnergy()) + 
					abs(pixels[y][x + 1].getEnergy() - pixels[y - 1][x].getEnergy());

				accumEnergy = std::min(std::min(Ml + Cl, Mu + Cu), Mr + Cr);
			}

			nextRow.push_back(accumEnergy);
		}

		energyMatrix.push_back(nextRow);
	}

	return energyMatrix;
};

std::vector<std::vector<int>> Image::generateForwardVerticalEnergyMatrix()
{
	std::vector<std::vector<int>> energyMatrix;

	for (int y = 0; y < Image::height; y++)
	{
		std::vector<int> nextRow;
		nextRow.push_back(pixels[y][0].getEnergy());
		energyMatrix.push_back(nextRow);
	}

	for (int x = 1; x < Image::width; x++)
	{
		for (int y = 0; y < Image::height; y++)
		{
			int accumEnergy = 0;
			if (y == 0)
			{
				accumEnergy = std::min(energyMatrix[y + 1][x - 1], energyMatrix[y][x - 1]) + pixels[y][x].getEnergy();
			}
			else if (y == Image::height - 1)
			{
				accumEnergy = std::min(energyMatrix[y - 1][x - 1], energyMatrix[y][x - 1]) + pixels[y][x].getEnergy();
			}
			else
			{
				int Mu = energyMatrix[y - 1][x - 1];
				int Mm = energyMatrix[y][x - 1];
				int Ml = energyMatrix[y + 1][x - 1];

				int Cu = abs(pixels[y - 1][x].getEnergy() - pixels[y + 1][x].getEnergy()) +
					abs(pixels[y - 1][x].getEnergy() - pixels[y][x - 1].getEnergy());
				int Cm = abs(pixels[y - 1][x].getEnergy() - pixels[y + 1][x].getEnergy());
				int Cl = abs(pixels[y - 1][x].getEnergy() - pixels[y + 1][x].getEnergy()) +
					abs(pixels[y + 1][x].getEnergy() - pixels[y][x - 1].getEnergy());

				accumEnergy = std::min(std::min(Ml + Cl, Mu + Cu), Mm + Cm);
			}
			energyMatrix[y].push_back(accumEnergy);
		}
	}

	return energyMatrix;
};

std::vector<int> Image::generateSeam()
{
	std::vector<std::vector<int>> energyMatrix = generateEnergyMatrix();

	std::vector<int> indexVector;

	int prevIndex = smallestValueIndex(energyMatrix[energyMatrix.size() - 1]);
	indexVector.push_back(prevIndex);

	for (int y = Image::height - 2; y >= 0; y--)
	{
		int currIndex = prevIndex;
		int val = energyMatrix[y][prevIndex];
		int val1 = prevIndex == 0 ? energyMatrix[y][prevIndex] : energyMatrix[y][prevIndex - 1];
		int val3 = prevIndex == energyMatrix[y].size() - 1 ? energyMatrix[y][prevIndex] : energyMatrix[y][prevIndex + 1];

		if (val1 < val)
		{
			val = val1;
			currIndex = prevIndex - 1;
		}
		if (val3 < val)
		{
			currIndex = prevIndex + 1;
		}

		indexVector.push_back(currIndex);
		prevIndex = currIndex;
	}

	return indexVector;
};

std::vector<int> Image::generateVerticalSeam()
{
	std::vector<std::vector<int>> energyMatrix = generateVerticalEnergyMatrix();

	std::vector<int> indexVector;
	std::vector<int> firstCol;

	for (int y = 0; y < Image::height; y++)
	{
		firstCol.push_back(energyMatrix[y][energyMatrix[0].size() - 1]);
	}

	int prevIndex = smallestValueIndex(firstCol);
	indexVector.push_back(prevIndex);

	for (int x = Image::width - 2; x >= 0; x--)
	{
		int currIndex = prevIndex;
		int val = energyMatrix[prevIndex][x];
		int val1 = prevIndex == 0 ? energyMatrix[prevIndex][x] : energyMatrix[prevIndex - 1][x];
		int val3 = prevIndex == energyMatrix.size() - 1 ? energyMatrix[prevIndex][x] : energyMatrix[prevIndex + 1][x];

		if (val1 < val)
		{
			val = val1;
			currIndex = prevIndex - 1;
		}
		if (val3 < val)
		{
			currIndex = prevIndex + 1;
		}

		indexVector.push_back(currIndex);
		prevIndex = currIndex;
	}

	return indexVector;
};

std::vector<std::vector<int>> Image::generateSeams(int diff)
{
	int limit = diff < Image::width / 5 ? diff : Image::width / 5;

	std::vector<std::vector<int>> seams;
	std::vector<std::vector<int>> energyMatrix = generateEnergyMatrix();
	for (int i = 0; i < limit; i++)
	{
		int start = smallestValueIndex(energyMatrix[Image::height - 1]);

		std::vector<int> seam;
		seam.push_back(start);

		int prevIndex = start;
		for (int y = Image::height - 2; y >= 0; y--)
		{
			int currIndex = prevIndex;
			int val = energyMatrix[y][prevIndex];
			int val1 = prevIndex == 0 ? energyMatrix[y][prevIndex] : energyMatrix[y][prevIndex - 1];
			int val3 = prevIndex == energyMatrix[y].size() - 1 ? energyMatrix[y][prevIndex] : energyMatrix[y][prevIndex + 1];

			if (val1 < val)
			{
				val = val1;
				currIndex = prevIndex - 1;
			}
			if (val3 < val)
			{
				currIndex = prevIndex + 1;
			}
			seam.push_back(currIndex);
			energyMatrix[y][currIndex] = std::numeric_limits<int>::max();
			prevIndex = currIndex;
		}
		seams.push_back(seam);
		energyMatrix[Image::height - 1][start] = std::numeric_limits<int>::max();
	}

	return seams;
};

std::vector<std::vector<int>> Image::generateVerticalSeams(int diff)
{
	int limit = diff < Image::height / 5 ? diff : Image::height / 5;

	std::vector<std::vector<int>> seams;
	std::vector<std::vector<int>> energyMatrix = generateVerticalEnergyMatrix();
	for (int i = 0; i < limit; i++)
	{
		std::vector<int> firstCol;

		for (int y = 0; y < Image::height; y++)
		{
			firstCol.push_back(energyMatrix[y][Image::width - 1]);
		}
		int start = smallestValueIndex(firstCol);

		std::vector<int> seam;
		seam.push_back(start);

		int prevIndex = start;
		for (int x = Image::width - 2; x >= 0; x--)
		{
			int currIndex = prevIndex;
			int val = energyMatrix[prevIndex][x];
			int val1 = prevIndex == 0 ? energyMatrix[prevIndex][x] : energyMatrix[prevIndex - 1][x];
			int val3 = prevIndex == energyMatrix.size() - 1 ? energyMatrix[prevIndex][x] : energyMatrix[prevIndex + 1][x];

			if (val1 < val)
			{
				val = val1;
				currIndex = prevIndex - 1;
			}
			if (val3 < val)
			{
				currIndex = prevIndex + 1;
			}
			seam.push_back(currIndex);
			energyMatrix[currIndex][x] = std::numeric_limits<int>::max();
			prevIndex = currIndex;
		}
		seams.push_back(seam);
		energyMatrix[start][Image::width - 1] = std::numeric_limits<int>::max();
	}

	return seams;
};

void Image::deleteSeam(std::vector<int> seam)
{
	for (int y = 0; y < Image::height; y++)
	{
		pixels[y].erase(pixels[y].begin() + seam[seam.size() - 1 - y]);
		//updateEnergy(seam[seam.size() - 1 - y], y);
	}
	Image::width--;
};

void Image::deleteVerticalSeam(std::vector<int> seam)
{
	for (int x = 0; x < Image::width; x++)
	{
		int id = seam[seam.size() - 1 - x];

		for (int y = id; y < Image::height - 1; y++)
		{
			pixels[y][x] = pixels[y + 1][x];
		}
		//updateEnergy(x, id);
	}
	pixels.erase(pixels.begin() + Image::height - 1);
	Image::height--;
};

void Image::addSeam(std::vector<int> seam)
{
	for (int y = 0; y < Image::height; y++)
	{
		int id = seam[seam.size() - 1 - y];

		Pixel toAdd = id == Image::width - 1 ? pixels[y][id] : interpolate(pixels[y][id], pixels[y][id + 1]);
		pixels[y].insert(pixels[y].begin() + id, toAdd);
		/*updateEnergy(id, y);
		if (id < Image::width - 1)
		{
			updateEnergy(id + 1, y);
		}*/
	}
	Image::width++;
};

void Image::addSeams(std::vector<std::vector<int>> seams)
{
	std::sort(seams.begin(), seams.end(), 
		[](const std::vector<int>& seam1, const std::vector<int> seam2)
		{
			return seam1[0] > seam2[0];
		});
	
	for (int i = 0; i < seams.size(); i++)
	{
		addSeam(seams[i]);
	}
};

void Image::addVerticalSeam(std::vector<int> seam)
{
	Image::height++;
	pixels.push_back(std::vector<Pixel>());
	pixels[pixels.size() - 1].resize(pixels[0].size());
	for (int x = 0; x < Image::width; x++)
	{
		int id = seam[seam.size() - 1 - x];

		Pixel toAdd = id == Image::height - 1 ? pixels[id][x] : interpolate(pixels[id][x], pixels[id + 1][x]);
		for (int y = Image::height - 1; y > id; y--)
		{
			pixels[y][x] = pixels[y - 1][x];
		}
		pixels[id][x] = toAdd; 
		/*updateEnergy(x, id);
		if (id < Image::height - 1)
		{
			updateEnergy(x, id + 1);
		}*/
	}
};

void Image::addVerticalSeams(std::vector<std::vector<int>> seams)
{
	std::sort(seams.begin(), seams.end(), 
		[](const std::vector<int>& seam1, const std::vector<int> seam2)
		{
			return seam1[0] > seam2[0];
		});
	

	for (int i = 0; i < seams.size(); i++)
	{
		addVerticalSeam(seams[i]);
	}
};

std::vector<int> Image::generateSeamForward()
{
	std::vector<std::vector<int>> energyMatrix = generateForwardEnergyMatrix();

	std::vector<int> indexVector;

	int prevIndex = smallestValueIndex(energyMatrix[energyMatrix.size() - 1]);
	indexVector.push_back(prevIndex);

	for (int y = Image::height - 2; y >= 0; y--)
	{
		int currIndex = prevIndex;
		int val = energyMatrix[y][prevIndex];
		int val1 = prevIndex == 0 ? energyMatrix[y][prevIndex] : energyMatrix[y][prevIndex - 1];
		int val3 = prevIndex == energyMatrix[y].size() - 1 ? energyMatrix[y][prevIndex] : energyMatrix[y][prevIndex + 1];

		if (val1 < val)
		{
			val = val1;
			currIndex = prevIndex - 1;
		}
		if (val3 < val)
		{
			currIndex = prevIndex + 1;
		}

		indexVector.push_back(currIndex);
		prevIndex = currIndex;
	}

	return indexVector;
};

std::vector<int> Image::generateVerticalSeamForward()
{
	std::vector<std::vector<int>> energyMatrix = generateForwardVerticalEnergyMatrix();

	std::vector<int> indexVector;
	std::vector<int> firstCol;

	for (int y = 0; y < Image::height; y++)
	{
		firstCol.push_back(energyMatrix[y][energyMatrix[0].size() - 1]);
	}

	int prevIndex = smallestValueIndex(firstCol);
	indexVector.push_back(prevIndex);

	for (int x = Image::width - 2; x >= 0; x--)
	{
		int currIndex = prevIndex;
		int val = energyMatrix[prevIndex][x];
		int val1 = prevIndex == 0 ? energyMatrix[prevIndex][x] : energyMatrix[prevIndex - 1][x];
		int val3 = prevIndex == energyMatrix.size() - 1 ? energyMatrix[prevIndex][x] : energyMatrix[prevIndex + 1][x];

		if (val1 < val)
		{
			val = val1;
			currIndex = prevIndex - 1;
		}
		if (val3 < val)
		{
			currIndex = prevIndex + 1;
		}

		indexVector.push_back(currIndex);
		prevIndex = currIndex;
	}

	return indexVector;
};

std::vector<std::vector<int>> Image::generateSeamsForward(int diff)
{
	int limit = diff < Image::width / 2 ? diff : Image::width / 2;

	std::vector<std::vector<int>> seams;
	std::vector<std::vector<int>> energyMatrix = generateForwardEnergyMatrix();
	for (int i = 0; i < limit; i++)
	{
		int start = smallestValueIndex(energyMatrix[Image::height - 1]);

		std::vector<int> seam;
		seam.push_back(start);

		int prevIndex = start;
		for (int y = Image::height - 2; y >= 0; y--)
		{
			int currIndex = prevIndex;
			int val = energyMatrix[y][prevIndex];
			int val1 = prevIndex == 0 ? energyMatrix[y][prevIndex] : energyMatrix[y][prevIndex - 1];
			int val3 = prevIndex == energyMatrix[y].size() - 1 ? energyMatrix[y][prevIndex] : energyMatrix[y][prevIndex + 1];

			if (val1 < val)
			{
				val = val1;
				currIndex = prevIndex - 1;
			}
			if (val3 < val)
			{
				currIndex = prevIndex + 1;
			}
			seam.push_back(currIndex);
			prevIndex = currIndex;
		}
		seams.push_back(seam);
		energyMatrix[Image::height - 1][start] = std::numeric_limits<int>::max();
	}

	return seams;
};

std::vector<std::vector<int>> Image::generateVerticalSeamsForward(int diff)
{
	int limit = diff < Image::height / 2 ? diff : Image::height / 2;

	std::vector<std::vector<int>> seams;
	std::vector<std::vector<int>> energyMatrix = generateForwardVerticalEnergyMatrix();
	for (int i = 0; i < limit; i++)
	{
		std::vector<int> firstCol;

		for (int y = 0; y < Image::height; y++)
		{
			firstCol.push_back(energyMatrix[y][Image::width - 1]);
		}
		int start = smallestValueIndex(firstCol);

		std::vector<int> seam;
		seam.push_back(start);

		int prevIndex = start;
		for (int x = Image::width - 2; x >= 0; x--)
		{
			int currIndex = prevIndex;
			int val = energyMatrix[prevIndex][x];
			int val1 = prevIndex == 0 ? energyMatrix[prevIndex][x] : energyMatrix[prevIndex - 1][x];
			int val3 = prevIndex == energyMatrix.size() - 1 ? energyMatrix[prevIndex][x] : energyMatrix[prevIndex + 1][x];

			if (val1 < val)
			{
				val = val1;
				currIndex = prevIndex - 1;
			}
			if (val3 < val)
			{
				currIndex = prevIndex + 1;
			}
			seam.push_back(currIndex);
			prevIndex = currIndex;
		}
		seams.push_back(seam);
		energyMatrix[start][Image::width - 1] = std::numeric_limits<int>::max();
	}

	return seams;
};
