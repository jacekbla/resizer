#include "pch.h"
#include "..//resizer//Image.h"

struct ImageTest : testing::Test
{
	Image* image;
	std::string inPath = "D:\\test\\testIn.jpg";
	std::string outPath = "D:\\test\\testOut.jpg";

	ImageTest()
	{
		image = new Image();
	}

	~ImageTest()
	{
		delete image;
	}
};


TEST_F(ImageTest, shrinkTest)
{
	image->init(inPath);
	int oldWidth = image->getWidth();

	cv::Mat3b img_file1 = cv::imread(inPath);
	cv::imshow("image", img_file1);
	cv::waitKey();

	image->shrink(100);

	EXPECT_EQ(image->getWidth(), oldWidth - 100);

	image->saveImage(outPath);
	cv::Mat3b img_file2 = cv::imread(outPath);
	cv::imshow("image", img_file2);
	cv::waitKey();
};

TEST_F(ImageTest, shrinkVerticalTest)
{
	image->init(inPath);
	int oldHeight = image->getHeight();

	cv::Mat3b img_file1 = cv::imread(inPath);
	cv::imshow("image", img_file1);
	cv::waitKey();

	image->shrinkVertical(100);

	EXPECT_EQ(image->getHeight(), oldHeight - 100);

	image->saveImage(outPath);
	cv::Mat3b img_file2 = cv::imread(outPath);
	cv::imshow("image", img_file2);
	cv::waitKey();
};

TEST_F(ImageTest, enlargeTest)
{
	image->init(inPath);
	int oldWidth = image->getWidth();

	cv::Mat3b img_file1 = cv::imread(inPath);
	cv::imshow("image", img_file1);
	cv::waitKey();

	image->enlarge(100);

	EXPECT_EQ(image->getWidth(), oldWidth + 100);

	image->saveImage(outPath);
	cv::Mat3b img_file2 = cv::imread(outPath);
	cv::imshow("image", img_file2);
	cv::waitKey();
};

TEST_F(ImageTest, enlargeVerticalTest)
{

	image->init(inPath);
	int oldHeight = image->getHeight();

	cv::Mat3b img_file1 = cv::imread(inPath);
	cv::imshow("image", img_file1);
	cv::waitKey();

	image->enlargeVertical(100);

	EXPECT_EQ(image->getHeight(), oldHeight + 100);

	image->saveImage(outPath);
	cv::Mat3b img_file2 = cv::imread(outPath);
	cv::imshow("image", img_file2);
	cv::waitKey();
};

TEST_F(ImageTest, shrinkSCTest)
{
	image->init(inPath);
	int oldWidth = image->getWidth();

	cv::Mat3b img_file1 = cv::imread(inPath);
	cv::imshow("image", img_file1);
	cv::waitKey();

	image->shrinkSC(400);

	EXPECT_EQ(image->getWidth(), oldWidth - 400);

	image->saveImage(outPath);
	cv::Mat3b img_file2 = cv::imread(outPath);
	cv::imshow("image", img_file2);
	cv::waitKey();
};

TEST_F(ImageTest, shrinkVerticalSCTest)
{
	image->init(inPath);
	int oldHeight = image->getHeight();

	cv::Mat3b img_file1 = cv::imread(inPath);
	cv::imshow("image", img_file1);
	cv::waitKey();

	image->shrinkVerticalSC(100);

	EXPECT_EQ(image->getHeight(), oldHeight - 100);

	image->saveImage(outPath);
	cv::Mat3b img_file2 = cv::imread(outPath);
	cv::imshow("image", img_file2);
	cv::waitKey();
};

TEST_F(ImageTest, enlargeSCTest)
{
	image->init(inPath);
	int oldWidth = image->getWidth();

	cv::Mat3b img_file1 = cv::imread(inPath);
	cv::imshow("image", img_file1);
	cv::waitKey();

	image->enlargeSC(100);

	EXPECT_EQ(image->getWidth(), oldWidth + 100);

	image->saveImage(outPath);
	cv::Mat3b img_file2 = cv::imread(outPath);
	cv::imshow("image", img_file2);
	cv::waitKey();
};

TEST_F(ImageTest, enlargeVerticalSCTest)
{

	image->init(inPath);
	int oldHeight = image->getHeight();

	cv::Mat3b img_file1 = cv::imread(inPath);
	cv::imshow("image", img_file1);
	cv::waitKey();

	image->enlargeVerticalSC(100);

	EXPECT_EQ(image->getHeight(), oldHeight + 100);

	image->saveImage(outPath);
	cv::Mat3b img_file2 = cv::imread(outPath);
	cv::imshow("image", img_file2);
	cv::waitKey();
};

TEST_F(ImageTest, shrinkForwardSCTest)
{
	image->init(inPath);
	int oldWidth = image->getWidth();

	cv::Mat3b img_file1 = cv::imread(inPath);
	cv::imshow("image", img_file1);
	cv::waitKey();

	image->shrinkForwardSC(100);

	EXPECT_EQ(image->getWidth(), oldWidth - 100);

	image->saveImage(outPath);
	cv::Mat3b img_file2 = cv::imread(outPath);
	cv::imshow("image", img_file2);
	cv::waitKey();
};

TEST_F(ImageTest, shrinkVerticalForwardSCTest)
{
	image->init(inPath);
	int oldHeight = image->getHeight();

	cv::Mat3b img_file1 = cv::imread(inPath);
	cv::imshow("image", img_file1);
	cv::waitKey();

	image->shrinkVerticalForwardSC(100);

	EXPECT_EQ(image->getHeight(), oldHeight - 100);

	image->saveImage(outPath);
	cv::Mat3b img_file2 = cv::imread(outPath);
	cv::imshow("image", img_file2);
	cv::waitKey();
};

TEST_F(ImageTest, enlargeForwardSCTest)
{
	image->init(inPath);
	int oldWidth = image->getWidth();

	cv::Mat3b img_file1 = cv::imread(inPath);
	cv::imshow("image", img_file1);
	cv::waitKey();

	image->enlargeForwardSC(100);

	EXPECT_EQ(image->getWidth(), oldWidth + 100);

	image->saveImage(outPath);
	cv::Mat3b img_file2 = cv::imread(outPath);
	cv::imshow("image", img_file2);
	cv::waitKey();
};

TEST_F(ImageTest, enlargeVerticalForwardSCTest)
{
	image->init(inPath);
	int oldHeight = image->getHeight();

	cv::Mat3b img_file1 = cv::imread(inPath);
	cv::imshow("image", img_file1);
	cv::waitKey();

	image->enlargeVerticalForwardSC(0);

	EXPECT_EQ(image->getHeight(), oldHeight);

	image->saveImage(outPath);
	cv::Mat3b img_file2 = cv::imread(outPath);
	cv::imshow("image", img_file2);
	cv::waitKey();
};



TEST_F(ImageTest, cropTest)
{
	image->init(inPath);

	int* points = new int[4];
	points[0] = 0;
	points[1] = 0;
	points[2] = image->getWidth() - 300;
	points[3] = image->getHeight();

	int oldWidth = image->getWidth();
	int oldHeight = image->getHeight();

	cv::Mat3b img_file1 = cv::imread(inPath);
	cv::imshow("image", img_file1);
	cv::waitKey();

	image->crop(points);

	EXPECT_EQ(image->getWidth(), abs(points[0] - points[2]));
	EXPECT_EQ(image->getHeight(), abs(points[1] - points[3]));

	image->saveImage(outPath);
	cv::Mat3b img_file2 = cv::imread(outPath);
	cv::imshow("image", img_file2);
	cv::waitKey();
};

TEST_F(ImageTest, deleteTest)
{
	image->init(inPath);

	int len = 8;
	int* points = new int[len];
	points[0] = 300;
	points[1] = 100;
	points[2] = 300;
	points[3] = 200;
	points[4] = 400;
	points[5] = 200;
	points[6] = 400;
	points[7] = 100;

	int oldWidth = image->getWidth();
	int oldHeight = image->getHeight();

	cv::Mat3b img_file1 = cv::imread(inPath);
	cv::imshow("image", img_file1);
	cv::waitKey();

	image->deleteObj(points, len);

	image->saveImage(outPath);
	cv::Mat3b img_file2 = cv::imread(outPath);
	cv::imshow("image", img_file2);
	cv::waitKey();
};

TEST_F(ImageTest, initTest)
{
	cv::Mat3b img_file1 = cv::imread(inPath);
	cv::imshow("image", img_file1);
	cv::waitKey();

	EXPECT_EQ(image->getWidth(), 0);
	EXPECT_EQ(image->getHeight(), 0);


	image->init(inPath);

	EXPECT_GT(image->getWidth(), 0);
	EXPECT_GT(image->getHeight(), 0);

	image->saveImage(outPath);
	cv::Mat3b img_file2 = cv::imread(outPath);
	cv::imshow("image", img_file2);
	cv::waitKey();
};



TEST(UtilityTest, smallestValueIndexTest)
{
	std::vector<int> vec{5,6,-10,7,123,-2,0,-8};

	int smallest = smallestValueIndex(vec);

	EXPECT_EQ(smallest, 2);
};

TEST(UtilityTest, smallestValueIndicesTest)
{
	std::vector<int> smallestValueIndices(std::vector<int> row);
	int smallestValueIndex(std::vector<int> vec);

	std::vector<int> vec{ 5,6,-10,7,123,-2,0,-8 };

	std::vector<int> sortedIndices = smallestValueIndices(vec);

	for (int i = 0; i < vec.size() - 1; i++)
	{
		EXPECT_LE(vec[sortedIndices[i]], vec[sortedIndices[i + 1]]);
	}
};

TEST(UtilityTest, isPointLeftOfLineTest)
{
	std::pair<int, int> point = std::pair<int, int>(100 ,100);

	std::pair<int, int> lineEnd1 = std::pair<int, int>(50, 50);
	std::pair<int, int> lineEnd2 = std::pair<int, int>(300, 200);

	EXPECT_TRUE(isPointLeftOfLine(point.first, point.second, lineEnd1.first, lineEnd1.second, lineEnd2.first, lineEnd2.second));


	lineEnd1 = std::pair<int, int>(200, 50);
	lineEnd2 = std::pair<int, int>(200, 90);

	EXPECT_FALSE(isPointLeftOfLine(point.first, point.second, lineEnd1.first, lineEnd1.second, lineEnd2.first, lineEnd2.second));


	lineEnd1 = std::pair<int, int>(200, 50);
	lineEnd2 = std::pair<int, int>(300, 300);

	EXPECT_TRUE(isPointLeftOfLine(point.first, point.second, lineEnd1.first, lineEnd1.second, lineEnd2.first, lineEnd2.second));


	lineEnd1 = std::pair<int, int>(50, 50);
	lineEnd2 = std::pair<int, int>(300, 300);

	EXPECT_FALSE(isPointLeftOfLine(point.first, point.second, lineEnd1.first, lineEnd1.second, lineEnd2.first, lineEnd2.second));


	lineEnd1 = std::pair<int, int>(100, 50);
	lineEnd2 = std::pair<int, int>(100, 200);

	EXPECT_TRUE(isPointLeftOfLine(point.first, point.second, lineEnd1.first, lineEnd1.second, lineEnd2.first, lineEnd2.second));


	lineEnd1 = std::pair<int, int>(200, 100);
	lineEnd2 = std::pair<int, int>(50, 100);

	EXPECT_FALSE(isPointLeftOfLine(point.first, point.second, lineEnd1.first, lineEnd1.second, lineEnd2.first, lineEnd2.second));
};

TEST(UtilityTest, dirPathTest)
{
	EXPECT_EQ(dirPath(), "D:\\BitBucket\\resizer\\cpp\\x64\\Release");
};