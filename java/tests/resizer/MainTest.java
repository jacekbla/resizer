package resizer;

import javafx.fxml.FXMLLoader;
import javafx.geometry.VerticalDirection;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;
import org.testfx.util.WaitForAsyncUtils;

import static javafx.scene.input.KeyCode.A;
import static javafx.scene.input.KeyCode.BACK_SPACE;
import static javafx.scene.input.KeyCode.CONTROL;

public class MainTest extends ApplicationTest {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("mainWindow.fxml"));
        primaryStage.setTitle("Resizer");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        //FxToolkit.hideStage();
        //release(new KeyCode[]{});
        //release(new MouseButton[]{});
        WaitForAsyncUtils.waitForFxEvents();
        sleep(1000);
    }

    @Test
    public void shrinkTest() {
        clickOn("#chooseFileButton");
        clickOn("#widthTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("123");
        clickOn("#heightTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("123");
        clickOn("#resizeButton");
    }

    @Test
    public void shrinkSCTest() {
        clickOn("#chooseFileButton");
        clickOn("#widthTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("123");
        clickOn("#heightTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("123");
        clickOn("#scSwitchButton");
        clickOn("#resizeButton");
    }


    @Test
    public void shrinkForwardSCTest() {
        clickOn("#chooseFileButton");
        clickOn("#widthTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("123");
        clickOn("#heightTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("123");
        clickOn("#scSwitchButton");
        clickOn("#forwardSwitchButton");
        clickOn("#resizeButton");
    }

    @Test
    public void enlargeTest() {
        clickOn("#chooseFileButton");
        clickOn("#widthTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("700");
        clickOn("#heightTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("700");
        clickOn("#resizeButton");
    }

    @Test
    public void enlargeSCTest() {
        clickOn("#chooseFileButton");
        clickOn("#heightTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("700");
        clickOn("#scSwitchButton");
        clickOn("#resizeButton");
    }


    @Test
    public void enlargeForwardSCTest() {
        clickOn("#chooseFileButton");
        clickOn("#widthTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("700");
        clickOn("#scSwitchButton");
        clickOn("#forwardSwitchButton");
        clickOn("#resizeButton");
    }

    @Test
    public void cropTest() {
        clickOn("#chooseFileButton");
        clickOn("#cropSwitchButton");
        moveBy(-300, -100);
        press(MouseButton.PRIMARY);
        moveBy(-100, -100);
        release(MouseButton.PRIMARY);
        clickOn("#specialButton");
    }

    @Test
    public void deleteTest() {
        clickOn("#chooseFileButton");
        clickOn("#deleteSwitchButton");
        moveBy(-300, -100);
        clickOn(MouseButton.PRIMARY);
        moveBy(-100, -100);
        clickOn(MouseButton.PRIMARY);
        moveBy(0,-100);
        clickOn(MouseButton.PRIMARY);
        moveBy(200,0);
        clickOn(MouseButton.PRIMARY);
        moveBy(10,0);
        clickOn(MouseButton.SECONDARY);
        clickOn("#specialButton");
    }

    @Test
    public void amplifyTest() {
        clickOn("#chooseFileButton");
        clickOn("#amplifySwitchButton");
        clickOn("#specialButton");
    }

    @Test
    public void resetTest() {
        clickOn("#chooseFileButton");
        clickOn("#widthTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("123");
        clickOn("#heightTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("123");
        clickOn("#scSwitchButton");
        clickOn("#resizeButton");
        clickOn("#amplifySwitchButton");
        clickOn("#specialButton");

        moveTo("#imageView");
        scroll(VerticalDirection.UP);
        press(MouseButton.PRIMARY);
        moveBy(300.0, 400.0);
        release(MouseButton.PRIMARY);

        clickOn("#resetButton");
    }

    @Test
    public void clearTest() {
        clickOn("#chooseFileButton");
        clickOn("#widthTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("123");
        clickOn("#heightTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("123");
        clickOn("#scSwitchButton");
        clickOn("#resizeButton");
        clickOn("#amplifySwitchButton");
        clickOn("#specialButton");

        moveTo("#imageView");
        scroll(VerticalDirection.UP);
        press(MouseButton.PRIMARY);
        moveBy(300.0, 400.0);
        release(MouseButton.PRIMARY);

        clickOn("#clearButton");
        clickOn("#chooseFileButton");
    }

    @Test
    public void saveTest() {
        clickOn("#chooseFileButton");
        clickOn("#widthTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("123");
        clickOn("#heightTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("123");
        clickOn("#scSwitchButton");
        clickOn("#resizeButton");

        clickOn("#saveAsTextField");
        write("test");
        clickOn("#saveButton");
    }

    @Test
    public void saveNoNameTest() {
        clickOn("#chooseFileButton");
        clickOn("#widthTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("500");
        clickOn("#heightTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("200");
        clickOn("#scSwitchButton");
        clickOn("#resizeButton");

        clickOn("#saveAsTextField");
        clickOn("#saveButton");
    }

    @Test
    public void saveBigNameTest() {
        clickOn("#chooseFileButton");
        clickOn("#widthTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("123");
        clickOn("#heightTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("123");
        clickOn("#scSwitchButton");
        clickOn("#resizeButton");

        clickOn("#saveAsTextField");
        write("tц!@12^ttesttesttesttesttesttesttesttesttesttest");
        clickOn("#saveButton");
    }

    @Test
    public void shrink1Test() {
        clickOn("#chooseFileButton");
        clickOn("#widthTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("1");
        clickOn("#heightTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("1");
        clickOn("#scSwitchButton");
        clickOn("#resizeButton");
    }

    @Test
    public void shrink0Test() {
        clickOn("#chooseFileButton");
        clickOn("#widthTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("0");
        clickOn("#heightTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("0");
        clickOn("#scSwitchButton");
        clickOn("#resizeButton");
    }

    @Test
    public void shrinkBigTest() {
        clickOn("#chooseFileButton");
        clickOn("#widthTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("025673896");
        clickOn("#heightTextField");
        push(CONTROL, A).push(BACK_SPACE);
        write("-87@541209");
        clickOn("#resizeButton");
    }

    @Test
    public void scrollTest() {
        clickOn("#chooseFileButton");
        moveTo("#imageView");
        press(MouseButton.PRIMARY);
        moveBy(200,200);
        release(MouseButton.PRIMARY);
        scroll(3, VerticalDirection.UP);
        press(MouseButton.PRIMARY);
        moveBy(-100,-200);
        release(MouseButton.PRIMARY);
        scroll(2, VerticalDirection.DOWN);
    }
}
