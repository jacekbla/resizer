package resizer;

import javafx.scene.image.Image;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.*;

public class ControllerTest {

    Controller controller;
    String testImagePath;

    @Before
    public void setUp() {
        controller = new Controller();
        testImagePath = "D:\\test\\cat2.jpg";
    }

    @After
    public void tearDown() {
    }

    @Test
    public void cppResize() {
        String path = controller.cppResize(testImagePath, 300, 300);

        try {
            Image image = new Image(new FileInputStream(new File(path)));
            assertEquals(image.getWidth(), 300, 0);
            assertEquals(image.getHeight(), 300, 0);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void cppResizeSC() {
        String path = controller.cppResizeSC(testImagePath, 300, 300);

        try {
            Image image = new Image(new FileInputStream(new File(path)));
            assertEquals(image.getWidth(), 300, 0);
            assertEquals(image.getHeight(), 300, 0);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void cppResizeForwardSC() {
        String path = controller.cppResizeForwardSC(testImagePath, 300, 300);

        try {
            Image image = new Image(new FileInputStream(new File(path)));
            assertEquals(image.getWidth(), 300, 0);
            assertEquals(image.getHeight(), 300, 0);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void cppCrop() {
        int[] points = {100, 100, 200, 200};
        String path = controller.cppCrop(testImagePath, points);

        try {
            Image image = new Image(new FileInputStream(new File(path)));
            assertEquals(image.getWidth(), 100, 0);
            assertEquals(image.getHeight(), 100, 0);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void cppDeleteObj() {
        int[] points = {100, 100, 200, 200, 150, 300};
        String path = controller.cppDeleteObj(testImagePath, points);

        try {
            Image imageOriginal = new Image(new FileInputStream(new File(testImagePath)));
            Image image = new Image(new FileInputStream(new File(path)));

            assertEquals(image.getWidth(), imageOriginal.getWidth() - 100, 0);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /*@Ignore
    @Test (expected = NullPointerException.class)
    public void initialize() {
        controller.initialize();
        assertTrue(controller.currShape.isEmpty());
        assertTrue(controller.currShapeScene.isEmpty());
        assertFalse(controller.shapeDone);
        assertFalse(controller.deleteMode);
        assertFalse(controller.amplifyMode);
        assertFalse(controller.cropMode);
        assertFalse(controller.sc);
        assertFalse(controller.forward);
        assertFalse(controller.isImageLoaded);
    }

    @Ignore
    @Test (expected = NullPointerException.class)
    public void chooseFile() {
        assertFalse(controller.isImageLoaded);
        controller.chooseFile();
        //Executable closureContainingCodeToTest = () ->controller.chooseFile();
        //assertThrows(NullPointerException.class, closureContainingCodeToTest);
        //assertThrows(FileNotFoundException.class, closureContainingCodeToTest);
        assertTrue(controller.isImageLoaded);
    }*/
}