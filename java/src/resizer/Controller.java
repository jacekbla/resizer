package resizer;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.io.*;
import java.util.ArrayList;

public class Controller {

    static {
        System.load(System.getProperty("user.dir") + "\\..\\cpp\\x64\\Release\\resizer.dll");
    }
    public native String cppResize(String path, int width, int height);
    public native String cppResizeSC(String path, int width, int height);
    public native String cppResizeForwardSC(String path, int width, int height);
    public native String cppCrop(String path, int[] points);
    public native String cppDeleteObj(String path, int[] points);

    private String inPath = System.getProperty("user.dir") + "\\..\\res\\in.jpg";
    private String ext = ".jpg";

    private Image currImage;
    private double currPosX;
    private double currPosY;
    private double imageCurrTranslateX;
    private double imageCurrTranslateY;
    private boolean deleteMode;
    private boolean cropMode;
    private boolean amplifyMode;

    private ArrayList<Integer> currShape;
    private ArrayList<Integer> currShapeScene;
    private boolean shapeDone;
    private boolean sc;
    private boolean forward;
    private boolean isImageLoaded;

    @FXML
    public ImageView imageView;

    @FXML
    public Pane imagePane;

    @FXML
    public TextField widthTextField;

    @FXML
    public TextField heightTextField;

    @FXML
    public TextField saveAsTextField;

    @FXML
    public Button specialButton;

    @FXML
    public CheckBox deleteSwitchButton;

    @FXML
    public CheckBox cropSwitchButton;

    @FXML
    public CheckBox amplifySwitchButton;

    @FXML
    public void initialize() {
        currShape = new ArrayList<>();
        currShapeScene = new ArrayList<>();
        shapeDone = false;
        deleteMode = false;
        amplifyMode = false;
        cropMode = false;
        sc = false;
        forward = false;
        isImageLoaded = false;

        specialButton.setDisable(true);

        widthTextField.textProperty().addListener((obs, oldText, newText) -> {
            if (!newText.matches("\\d*")) {
                widthTextField.setText(newText.replaceAll("[^\\d]", ""));
            }
            else {
                if (newText.length() >= 5 && oldText.length() < 5) {
                    widthTextField.setText(oldText);
                }
            }
        });
        heightTextField.textProperty().addListener((obs, oldText, newText) -> {
            if (!newText.matches("\\d*")) {
                heightTextField.setText(newText.replaceAll("[^\\d]", ""));
            }
            else {
                if (oldText.length() < 5 && newText.length() >= 5) {
                    heightTextField.setText(oldText);
                }
            }
        });
        saveAsTextField.textProperty().addListener((obs, oldText, newText) -> {
            if(!newText.matches("^[a-zA-Z]+$")) {
                saveAsTextField.setText(newText.replaceAll("[^a-zA-Z]", ""));
            }
            else {
                if (oldText.length() < 10 && newText.length() >= 10) {
                    saveAsTextField.setText(oldText);
                }
            }
        });


        imageView.setOnScroll(event -> {
            if(event.getDeltaY() < 0) {
                zoomIn();
            }
            else {
                zoomOut();
            }
        });
        imageView.addEventHandler(MouseEvent.MOUSE_DRAGGED, imageOnMouseDraggedEventHandler);
        imageView.addEventHandler(MouseEvent.MOUSE_PRESSED, imageOnMousePressedEventHandler);
    }

    private EventHandler<MouseEvent> imageOnMousePressedEventHandler =
            new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    currPosX = event.getSceneX();
                    currPosY = event.getSceneY();
                    imageCurrTranslateX = imageView.getTranslateX();
                    imageCurrTranslateY = imageView.getTranslateY();
                }
            };

    private EventHandler<MouseEvent> imageOnMouseDraggedEventHandler =
            new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    double offsetX = event.getSceneX() - currPosX;
                    double offsetY = event.getSceneY() - currPosY;
                    double newTranslateX = imageCurrTranslateX + offsetX;
                    double newTranslateY = imageCurrTranslateY + offsetY;

                    imageView.setTranslateX(newTranslateX);
                    imageView.setTranslateY(newTranslateY);
                    deleteCurrShape();
                }
            };

    private EventHandler<MouseEvent> imageCropOnMousePressedEventHandler =
            new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    deleteCurrShape();
                    currShape.add((int)event.getX());
                    currShape.add((int)event.getY());
                    currShapeScene.add((int)event.getSceneX());
                    currShapeScene.add((int)event.getSceneY());
                }
            };

    private EventHandler<MouseEvent> imageCropOnMouseReleasedEventHandler =
            new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    int xToAdd = (int)event.getX();
                    int yToAdd = (int)event.getY();

                    if(xToAdd > imageView.getFitWidth()) {
                        xToAdd = (int)imageView.getFitWidth() - 1;
                    }
                    if(xToAdd < 0) {
                        xToAdd = 0;
                    }
                    if(yToAdd > imageView.getFitHeight()) {
                        yToAdd = (int)imageView.getFitHeight() - 1;
                    }
                    if(yToAdd < 0) {
                        yToAdd = 0;
                    }

                    currShape.add(xToAdd);
                    currShape.add(yToAdd);
                    currShapeScene.add((int)event.getSceneX());
                    currShapeScene.add((int)event.getSceneY());
                    specialButton.setDisable(false);

                    Line line1 = new Line(currShapeScene.get(0), currShapeScene.get(1), currShapeScene.get(0), currShapeScene.get(3));
                    Line line2 = new Line(currShapeScene.get(0), currShapeScene.get(1), currShapeScene.get(2), currShapeScene.get(1));
                    Line line3 = new Line(currShapeScene.get(2), currShapeScene.get(3), currShapeScene.get(2), currShapeScene.get(1));
                    Line line4 = new Line(currShapeScene.get(2), currShapeScene.get(3), currShapeScene.get(0), currShapeScene.get(3));

                    line1.setStrokeWidth(3.0);
                    line1.setStroke(Color.BLUE);
                    line2.setStrokeWidth(3.0);
                    line2.setStroke(Color.BLUE);
                    line3.setStrokeWidth(3.0);
                    line3.setStroke(Color.BLUE);
                    line4.setStrokeWidth(3.0);
                    line4.setStroke(Color.BLUE);
                    imagePane.getChildren().add(line1);
                    imagePane.getChildren().add(line2);
                    imagePane.getChildren().add(line3);
                    imagePane.getChildren().add(line4);
                }
            };

    private EventHandler<MouseEvent> imageOnMouseClickedEventHandler =
            new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(event.getButton() == MouseButton.PRIMARY){
                        if(!shapeDone) {
                            if(!currShapeScene.isEmpty()) {
                                Line line = new Line(event.getSceneX(), event.getSceneY(),
                                        currShapeScene.get(currShapeScene.size() - 2),
                                        currShapeScene.get(currShapeScene.size() - 1));
                                line.setStrokeWidth(3.0);
                                line.setStroke(Color.RED);
                                imagePane.getChildren().add(line);
                            }
                            currShape.add((int)event.getX());
                            currShape.add((int)event.getY());

                            currShapeScene.add((int)event.getSceneX());
                            currShapeScene.add((int)event.getSceneY());
                        }
                    }
                    else if(event.getButton() == MouseButton.SECONDARY) {
                        if(currShape.size() > 4) {
                            Line line = new Line(currShapeScene.get(0), currShapeScene.get(1),
                                    currShapeScene.get(currShapeScene.size() - 2),
                                    currShapeScene.get(currShapeScene.size() - 1));
                            line.setStrokeWidth(3.0);
                            line.setStroke(Color.RED);
                            imagePane.getChildren().add(line);
                            specialButton.setDisable(false);
                            shapeDone = true;
                        }
                    }
                }
            };

    @FXML
    public void chooseFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        File file = fileChooser.showOpenDialog(new Stage());//new File("D:\\test\\cat2.jpg");
        if(file!=null) {
            String extTemp = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf('.'));
            if(extTemp.equals(".jpg") || extTemp.equals(".png") || extTemp.equals(".bmp")) {
                try {
                    InputStream fileStream = new FileInputStream(file);
                    currImage = new Image(fileStream);
                    imageView.setImage(currImage);
                    fitImage(currImage);
                    updateTextFields(currImage.getWidth(), currImage.getHeight());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                isImageLoaded = true;
                ext = extTemp;
                inPath = System.getProperty("user.dir") + "\\..\\res\\in" + ext;
                saveAsTextField.setText("");
                deleteCurrShape();
            }
        }
    }

    @FXML
    public void saveFile() {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Save Image");
        File defaultDirectory = new File(System.getProperty("user.dir"));
        chooser.setInitialDirectory(defaultDirectory);
        File selectedDirectory = chooser.showDialog(new Stage());//new File("D:\\test");

        if(selectedDirectory != null) {
            Image image = imageView.getImage();
            if(image != null) {
                try {
                    String fileName = saveAsTextField.getText().equals("") ? "resizerFile" : saveAsTextField.getText();
                    ImageIO.write(SwingFXUtils.fromFXImage(image, null), ext.substring(1),
                            new File(selectedDirectory.getAbsolutePath() + "\\" + fileName + ext));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @FXML
    public void resize() {
        if(isImageLoaded && !widthTextField.getText().equals("") && !heightTextField.getText().equals("") &&
                Integer.parseInt(widthTextField.getText()) > 1 && Integer.parseInt(heightTextField.getText()) > 1) {
            int width = Integer.parseInt(widthTextField.getText());
            int height = Integer.parseInt(heightTextField.getText());

            saveImage();

            String outPath = sc ? forward ? cppResizeForwardSC(inPath, width, height) : cppResizeSC(inPath, width, height) : cppResize(inPath, width, height);

            changeImage(outPath);
        }
    }

    @FXML
    private void amplify() {
        if(isImageLoaded) {
            Image image = saveImage();

            double oldWidth = image.getWidth();
            double oldHeight = image.getHeight();
            double newWidth = 1.2 * oldWidth;
            double newHeight = 1.2 * oldHeight;

            String outPath = cppResize(inPath, (int) newWidth, (int) newHeight);
            outPath = forward ? cppResizeForwardSC(outPath, (int) oldWidth, (int) oldHeight) : cppResizeSC(outPath, (int) oldWidth, (int) oldHeight);

            changeImage(outPath);
        }
    }

    @FXML
    private void crop() {
        if(isImageLoaded) {
            saveImage();
            int[] points = new int[currShape.size()];
            for (int i = 0; i < currShape.size(); i++) {
                points[i] = currShape.get(i);
            }
            String outPath = cppCrop(inPath, points);

            changeImage(outPath);

            specialButton.setDisable(true);
            currShape.clear();
        }
    }

    @FXML
    private void deleteObj() {
        if(isImageLoaded) {
            saveImage();

            int[] points = new int[currShape.size()];
            for (int i = 0; i < currShape.size(); i++) {
                points[i] = currShape.get(i);
            }
            String outPath = cppDeleteObj(inPath, points);

            changeImage(outPath);

            specialButton.setDisable(true);
            currShape.clear();
            shapeDone = false;
        }
    }

    @FXML
    public void switchAmplifyMode() {
        if(amplifyMode) {
            amplifyMode = false;

            specialButton.setDisable(true);
            specialButton.setText("Action");
            specialButton.setOnAction(null);

            cropSwitchButton.setDisable(false);
            deleteSwitchButton.setDisable(false);
        }
        else {
            amplifyMode = true;

            specialButton.setDisable(false);
            specialButton.setText("Amplify");
            specialButton.setOnAction(event -> amplify());

            cropSwitchButton.setDisable(true);
            deleteSwitchButton.setDisable(true);
        }
    }

    @FXML
    public void switchCropMode() {
        if(cropMode) {
            cropMode = false;

            specialButton.setDisable(true);
            specialButton.setText("Action");
            specialButton.setOnAction(null);

            deleteSwitchButton.setDisable(false);
            amplifySwitchButton.setDisable(false);

            imageView.addEventHandler(MouseEvent.MOUSE_DRAGGED, imageOnMouseDraggedEventHandler);
            imageView.addEventHandler(MouseEvent.MOUSE_PRESSED, imageOnMousePressedEventHandler);
            imageView.removeEventHandler(MouseEvent.MOUSE_PRESSED, imageCropOnMousePressedEventHandler);
            imageView.removeEventHandler(MouseEvent.MOUSE_RELEASED, imageCropOnMouseReleasedEventHandler);
            deleteCurrShape();
        }
        else {
            cropMode = true;

            specialButton.setText("Crop");
            specialButton.setOnAction(event -> crop());

            deleteSwitchButton.setDisable(true);
            amplifySwitchButton.setDisable(true);

            imageView.removeEventHandler(MouseEvent.MOUSE_DRAGGED, imageOnMouseDraggedEventHandler);
            imageView.removeEventHandler(MouseEvent.MOUSE_PRESSED, imageOnMousePressedEventHandler);
            imageView.addEventHandler(MouseEvent.MOUSE_PRESSED, imageCropOnMousePressedEventHandler);
            imageView.addEventHandler(MouseEvent.MOUSE_RELEASED, imageCropOnMouseReleasedEventHandler);
            deleteCurrShape();
        }
    }

    @FXML
    public void switchDeleteMode() {
        if(deleteMode) {
            deleteMode = false;

            specialButton.setDisable(true);
            specialButton.setText("Action");
            specialButton.setOnAction(null);

            cropSwitchButton.setDisable(false);
            amplifySwitchButton.setDisable(false);

            imageView.removeEventHandler(MouseEvent.MOUSE_CLICKED, imageOnMouseClickedEventHandler);
            imageView.addEventHandler(MouseEvent.MOUSE_DRAGGED, imageOnMouseDraggedEventHandler);
            deleteCurrShape();
        }
        else {
            deleteMode = true;

            specialButton.setText("Delete");
            specialButton.setOnAction(event -> deleteObj());

            cropSwitchButton.setDisable(true);
            amplifySwitchButton.setDisable(true);

            imageView.removeEventHandler(MouseEvent.MOUSE_DRAGGED, imageOnMouseDraggedEventHandler);
            imageView.addEventHandler(MouseEvent.MOUSE_CLICKED, imageOnMouseClickedEventHandler);
            deleteCurrShape();
        }
    }

    @FXML
    public void switchSeamCarving() {
        sc = !sc;
    }

    @FXML
    public void switchForward() {
        forward = !forward;
    }

    @FXML
    public void reset() {
        if(isImageLoaded) {
            imageView.setScaleX(1.0);
            imageView.setScaleY(1.0);
            imageView.setTranslateX(0.0);
            imageView.setTranslateY(0.0);
            imageView.setImage(currImage);
            fitImage(currImage);
            updateTextFields(currImage.getWidth(), currImage.getHeight());
            deleteCurrShape();
        }
    }

    @FXML
    public void clear() {
        imageView.setImage(null);
        imageView.setScaleX(1.0);
        imageView.setScaleY(1.0);
        imageView.setTranslateX(0.0);
        imageView.setTranslateY(0.0);
        deleteCurrShape();
        currImage = null;
        isImageLoaded = false;

        widthTextField.clear();
        heightTextField.clear();

        saveAsTextField.setText("");
    }

    private void zoomIn() {
        imageView.setScaleX(imageView.getScaleX() - 0.05f);
        imageView.setScaleY(imageView.getScaleY() - 0.05f);
    }

    private void zoomOut() {
        imageView.setScaleX(imageView.getScaleX() + 0.05f);
        imageView.setScaleY(imageView.getScaleY() + 0.05f);
    }

    private void fitImage(Image image) {
        imageView.setFitWidth(image.getWidth());
        imageView.setFitHeight(image.getHeight());
        if(imageView.getFitWidth() > imagePane.getWidth()) {
            double scale = imagePane.getWidth() / imageView.getFitWidth();
            imageView.setScaleX(scale);
            imageView.setScaleY(scale);

            double diffX = (imageView.getFitWidth() - imagePane.getWidth()) / 2;
            double diffY = (imageView.getFitHeight() - imagePane.getHeight()) / 2;

            imageView.setTranslateX(-diffX);
            imageView.setTranslateY(-diffY);
        }
        else if(imageView.getFitHeight() > imagePane.getHeight()) {
            double scale = imagePane.getHeight() / imageView.getFitHeight();
            imageView.setScaleX(scale);
            imageView.setScaleY(scale);

            double diffX = (imageView.getFitWidth() - imagePane.getWidth()) / 2;
            double diffY = (imageView.getFitHeight() - imagePane.getHeight()) / 2;

            imageView.setTranslateX(-diffX);
            imageView.setTranslateY(-diffY);
        }
    }

    private void changeImage(String path) {
        try {
            InputStream fileStream = new FileInputStream(path);

            Image resizedImage = new Image(fileStream);
            imageView.setTranslateX(0.0);
            imageView.setTranslateY(0.0);
            imageView.setScaleX(1.0);
            imageView.setScaleY(1.0);
            imageView.setImage(resizedImage);
            fitImage(resizedImage);
            updateTextFields(resizedImage.getWidth(), resizedImage.getHeight());
            deleteCurrShape();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Image saveImage() {
        Image image = imageView.getImage();
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), ext.substring(1), new File(inPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;
    }

    private void deleteCurrShape() {
        imagePane.getChildren().remove(1, imagePane.getChildren().size());
        currShape.clear();
        currShapeScene.clear();
        shapeDone = false;
    }

    private void updateTextFields(double width, double height) {
        widthTextField.setText(Integer.toString((int)width));
        heightTextField.setText(Integer.toString((int)height));
    }
}
